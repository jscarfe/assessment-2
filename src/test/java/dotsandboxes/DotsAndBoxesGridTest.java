package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     */
    
     // Unit test to determine if the boxComplete algorithm correctly discerns a box is complete
     // This test is currently vacuously true - it's not failing but that isn't indicative of 
     // the algorithm working - the function simply always returns true for values of x and y where
     // 0 < x < width-1 OR 0 < y < height-1
    @Test
    public void testBoxIsComplete() {
        // test against box that is known to be complete
        DotsAndBoxesGrid test1 = new DotsAndBoxesGrid(4, 4, 2);
        test1.drawHorizontal(0, 0, 0);
        test1.drawVertical(0, 0, 1);
        test1.drawHorizontal(0, 1, 0);  
        test1.drawVertical(1, 0, 1);
        assertTrue(test1.boxComplete(0, 0));
        }

    // Unit test to determine if the boxComplete correctly discerns a box is not complete
    @Test 
    public void testBoxIsNotComplete() {
        // test against 'box' that is known to not be complete
        DotsAndBoxesGrid test2 = new DotsAndBoxesGrid(6, 6, 2);
        test2.drawHorizontal(0, 0, 0);
        test2.drawVertical(0, 0, 1);
        test2.drawHorizontal(2, 0, 0);  
        test2.drawVertical(2, 0, 1);
        assertFalse(test2.boxComplete(0, 0));
    }   

    // Unit test to determine if the horizontal draw function permits you to draw over a line that has already been drawn
    @Test
    public void testHorizontalLineAlreadyDrawn() {
        DotsAndBoxesGrid test = new DotsAndBoxesGrid(5, 5, 2);
        test.drawHorizontal(1, 0, 0);
        assertThrows(IllegalStateException.class, () -> {
            test.drawHorizontal(1, 0, 0); 
          });
    }
    // Unit test to determine if the vertical draw function permits you to draw over a line that has already been drawn
    @Test
    public void testVerticalLineAlreadyDrawn() {
        DotsAndBoxesGrid test = new DotsAndBoxesGrid(5, 5, 2);
        test.drawVertical(1, 0, 0);
        assertThrows(IllegalStateException.class, () -> {
            test.drawVertical(1, 0, 0); 
          });
    }
}
